const birthdate = document.querySelector("#birthdate");
const education = document.querySelectorAll(".education input");

const changeBGColor = (id) => {
  document.getElementById(id).style.backgroundColor = "red";
};

const validationForm = (ev) => {
  const personalData = validationPersonalData();
  const direction = validationDirection();
  const account = validationBankAccount();
  const textArea = ValidateTextArea();

  if (personalData && direction && account && textArea) {
    alert("El formulario está correcto!");
  } else {
    ev.preventDefault();
    alert("El formulario tiene fallos...");
  }
};

const validationPersonalData = () => {
  const fname = checkFirstName();
  const lname = checkLastName();
  const age = checkAge();
  const genre = checkGenre();
  const email = checkEmail();

  if (fname && lname && age && genre && email) {
    return true;
  }
  return false;
};

const validationDirection = () => {
  const address = checkAddress();
  const state = checkState();

  if (state && address) {
    return true;
  }

  return false;
};

const validationBankAccount = () => {
  const creditCard = checkCreditCard();
  const ccType = checkCreditCardType();

  if (creditCard && ccType) {
    return true;
  }

  return false;
};

const checkCreditCard = () => {
  const creditCard = document.querySelector("#creditCard");
  creditCard.style.backgroundColor = "white";
  const numberCreditCard = creditCard.value;

  if (numberCreditCard.length === 4) {
    let array = numberCreditCard.split(" ");
    for (let index = 0; index < array.length; index++) {
      if (array[index] !== 4) {
        alert("La targeta no es correcta");
        return false;
      }
    }
    return true;
  }
  creditCard.style.backgroundColor = "red";
  alert("La targeta no es correcta");

  return false;
};

const checkCreditCardType = () => {
  const cardType = document.querySelectorAll(".cardType input");
  if (cardType[0].checked || cardType[1].checked) {
    return true;
  }
  alert("Seleccione el tipo de targeta, por favor");
  return false;
};

const ValidateTextArea = () => {
  const textarea = document.querySelector("#moreInfo");
  textarea.style.backgroundColor = "white";

  if (textarea.value.length >= 10) {
    return true;
  }
  alert("El apartado de información tiene que contener al menos 10 carácteres");
  textarea.style.backgroundColor = "red";
  return false;
};

const checkAddress = () => {
  const address = document.querySelector("#address");

  if (address.value !== "") {
    return true;
  }
  alert("La dirección está vacía");
  return false;
};

const checkState = () => {
  const state = document.querySelector("#state");

  if (state.value !== "") {
    return true;
  }
  alert("La población está vacía");
  return false;
};

const checkFirstName = () => {
  const firstName = document.querySelector("#firstName");
  firstName.style.backgroundColor = "white";

  if (firstName.value !== "") {
    return true;
  }
  alert("El nombre está vacío");
  firstName.style.backgroundColor = "red";

  return false;
};

const checkLastName = () => {
  const lastName = document.querySelector("#lastName");

  if (lastName.value !== "") {
    lastName.style.backgroundColor = "white";

    return true;
  }
  alert("El apellido está vacío");
  lastName.style.backgroundColor = "red";

  return false;
};

const checkAge = () => {
  const age = document.querySelector("#age");
  age.style.backgroundColor = "white";

  if (age.value <= 0 || isNaN(age.value)) {
    alert("La edad no es válida");
    age.style.backgroundColor = "red";

    return false;
  }
  return true;
};

const checkGenre = () => {
  const genre = document.querySelectorAll(".genre input");
  if (genre[0].checked || genre[1].checked) {
    return true;
  }
  alert("Seleccione un género, por favor");
  return false;
};

const checkEmail = () => {
  const email = document.querySelector("#email");
  const emailConfirm = document.querySelector("#emailConfirm");
  email.style.backgroundColor = "white";
  emailConfirm.style.backgroundColor = "white";

  if (email.value == emailConfirm.value) {
    return true;
  } else {
    alert("La direcciones de email no coinciden");
    email.style.backgroundColor = "red";
    emailConfirm.style.backgroundColor = "red";

    return false;
  }
};

// Tarea para mañana, realiza todas las comprobaciones del formulario, vas por muy buen camino, guíate de lo que pide el examen y ya está, además de seguir el cómo lo han hecho Facu y Héctor
