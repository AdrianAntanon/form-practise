"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.Coordenades = exports.APICercalia = void 0;
var fetch = require("node-fetch");
var BASE_URL = "http://lb.cercalia.com/services/json?key=138aae8f9e2586cf8ff4b872cd26eee16825ee9c92ab932fddbfc6d49daabb28&";
var APICercalia = /** @class */ (function () {
    function APICercalia(calle, numero, codigo_postal, poblacion, codigo_pais) {
        this.calle = calle;
        this.num = numero;
        this.cod_postal = codigo_postal;
        this.poblacion = poblacion;
        this.cod_pais = codigo_pais;
    }
    APICercalia.prototype.getCoordernades = function () {
        return __awaiter(this, void 0, void 0, function () {
            var address_1, info, error_1, myError;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        return [4 /*yield*/, fetch(BASE_URL + "&cmd=cand&detcand=1&adr=" + this.calle + " " + this.num + "&ctn=" + this.poblacion + "&pcode=" + this.cod_postal + "&ctryc=" + this.cod_pais)];
                    case 1:
                        address_1 = _a.sent();
                        return [4 /*yield*/, address_1.json()];
                    case 2:
                        info = _a.sent();
                        // console.log(await address.json());
                        return [2 /*return*/, info];
                    case 3:
                        error_1 = _a.sent();
                        myError = new Error("La dirección no existe o no ha sido encontrada");
                        throw myError;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    return APICercalia;
}());
exports.APICercalia = APICercalia;
var Coordenades = /** @class */ (function () {
    function Coordenades() {
    }
    return Coordenades;
}());
exports.Coordenades = Coordenades;
var address = new APICercalia("verdum", "33", 8042, "barcelona", "ESP");
var direction = address.getCoordernades();
direction.then(function (result) {
    console.log("Las posibles direcciones encontradas son: ");
    for (var key in result.cercalia.candidates.candidate) {
        if (Object.prototype.hasOwnProperty.call(result.cercalia.candidates.candidate, key)) {
            var element = result.cercalia.candidates.candidate[key];
            console.log(element);
        }
    }
});
