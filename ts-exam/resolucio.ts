const fetch = require("node-fetch");

const BASE_URL = "http://lb.cercalia.com/services/json?key=138aae8f9e2586cf8ff4b872cd26eee16825ee9c92ab932fddbfc6d49daabb28&";

export class APICercalia {
  calle: string;
  num: string;
  cod_postal: number;
  poblacion: string;
  cod_pais: string;
  constructor(calle: string, numero: string, codigo_postal: number, poblacion: string, codigo_pais: string) {
    this.calle = calle;
    this.num = numero;
    this.cod_postal = codigo_postal;
    this.poblacion = poblacion;
    this.cod_pais = codigo_pais;
  }

  async getCoordernades() {
    try {
      const address = await fetch(`${BASE_URL}&cmd=cand&detcand=1&adr=${this.calle} ${this.num}&ctn=${this.poblacion}&pcode=${this.cod_postal}&ctryc=${this.cod_pais}`);

      const info = await address.json();

      // console.log(await address.json());

      return info;

    } catch (error) {
      const myError = new Error("La dirección no existe o no ha sido encontrada");
      throw myError;
    }
  }
}

export class Coordenades {

}


const address = new APICercalia("verdum", "33", 8042, "barcelona", "ESP");

const direction = address.getCoordernades();


direction.then((result) => {
  console.log("Las posibles direcciones encontradas son: ");

  for (const key in result.cercalia.candidates.candidate) {
    if (Object.prototype.hasOwnProperty.call(result.cercalia.candidates.candidate, key)) {
      const element = result.cercalia.candidates.candidate[key];
      console.log(element);
    }
  }
});