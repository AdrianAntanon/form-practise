const changeBGColor = (id) => {
  document.getElementById(id).style.backgroundColor = "red";
};

const validateForm = ev => {

  const personalData = validatePersonalData();
  const personalAddress = validatePersonalAddress();
  const personalBankAccount = validateBankAccount();

  if (personalData && personalAddress && personalBankAccount) {
    alert("Formulario validado, está aprobado");

    return;
  }

  alert("El formulario contiene errores o falta por rellenar correctamente algún campo");
  ev.preventDefault();

};

const checkEmptyInput = inputValue => {

  if (inputValue !== "") {
    return true;
  }

  return false;

};

const checkNumber = inputValue => {
  if (inputValue <= 0 || isNaN(inputValue)) {
    return false;
  }

  return true;
};

// personalData
const validatePersonalData = () => {
  const firstNameInput = document.querySelector("#firstName").value;
  const lastNameInput = document.querySelector("#lastName").value;
  const ageInput = document.querySelector("#age").value;

  const firstName = checkEmptyInput(firstNameInput);
  const lastName = checkEmptyInput(lastNameInput);
  const age = checkNumber(ageInput);
  const email = checkEmail();
  const genre = checkGenre();

  if (firstName && lastName && age && email && genre) {
    return true;
  }

  return false;

};

const checkGenre = () => {
  const genre = document.querySelectorAll(".genre input");

  if (genre[0].checked || genre[1].checked) {
    return true;
  }

  return false;
};

const checkEmail = () => {
  const email = document.querySelector("#email").value;
  const emailConfirm = document.querySelector("#emailConfirm").value;

  if (email === "" || emailConfirm === "") {
    return false;
  }

  if (email === emailConfirm) {
    return true;
  }

  return false;
};

// personalAddress
const validatePersonalAddress = () => {
  const addressInput = document.querySelector("#address").value;
  const stateInput = document.querySelector("#state").value;
  const zipCodeInput = document.querySelector("#zipCode").value;

  const address = checkEmptyInput(addressInput);
  const state = checkEmptyInput(stateInput);
  const zipCode = checkNumber(zipCodeInput);

  if (address && state && zipCode) {
    return true;
  }

  return false;
};

// personalBankAccount

const validateBankAccount = () => {
  const cc = checkCreditCard();
  const cct = checkCreditCardType();

  if (cc && cct) {
    return true;
  }

  return false;
};

const checkCreditCard = () => {
  const creditCard = document.querySelector("#creditCard").value;
  if (creditCard === "") {
    return false;
  }

  const creditCardArray = creditCard.split(" ");

  if (creditCardArray.length === 4) {
    for (let index = 0; index < creditCardArray.length; index++) {
      if (creditCardArray[index].length !== 4) return false;
    }
    return true;
  }

  return false;
};

const checkCreditCardType = () => {
  const creditCardType = document.querySelectorAll(".creditCardType input");

  if (creditCardType[0].checked || creditCardType[1].checked) {
    return true;
  }

  return false;
};