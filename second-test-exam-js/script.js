const changeBGColor = (id) => {
  document.getElementById(id).style.backgroundColor = "red";
};

const validateForm = (ev) => {
  const personalData = validatePersonalInfo();
  const personalAddress = validateAddress();
  const personalAccount = validateBankAccount();

  if (personalData && personalAddress && personalAccount) {
    alert("Formulario validado, estás aprobado");
    return;
  }

  alert("El formulario contiene errores...");
  ev.preventDefault();
};


// Comprobar información personal

const validatePersonalInfo = () => {
  const firstName = document.querySelector("#firstName");
  const lastName = document.querySelector("#lastName");

  const age = checkAge();
  const fName = checkName(firstName);
  const lName = checkName(lastName);
  const email = checkEmail();
  const genre = checkGenre();

  if (fName && lName && age && email && genre) {
    return true;
  }

  return false;



};

const checkName = (inputValue) => {
  const name = inputValue.value;
  if (name !== "") {
    return true;
  }
  return false;
};

const checkAge = () => {
  const age = document.querySelector("#age").value;

  if (age <= 0 || isNaN(age)) {
    return false;
  }

  return true;
};

const checkEmail = () => {
  const email = document.querySelector("#email");
  const emailConfirm = document.querySelector("#emailConfirm");


  if (email.value == "" || emailConfirm.value == "") {

    return false;
  }

  if (email.value === emailConfirm.value) {
    return true;
  }


  return false;
};

const checkGenre = () => {
  const genre = document.querySelectorAll(".genre input");

  if (genre[0].checked || genre[1].checked) {
    return true;
  }

  return false;
};

// Comprobar dirección
const validateAddress = () => {
  const direction = document.querySelector("#direction");
  const state = document.querySelector("#state");

  const myDir = checkName(direction);
  const myState = checkName(state);
  const zipCode = checkZipCode();


  if (myDir && myState && zipCode) {
    return true;
  }

  return false;
};

const checkZipCode = () => {
  const zipCode = document.querySelector("#zipCode").value;

  if (zipCode) {
    return true;
  }

  return false;
};

// Comprobar datos bancarios
const validateBankAccount = () => {
  const creditCard = document.querySelector("#creditCard");

  const numCreditCard = creditCard.value;



  let arrayCreditCard = numCreditCard.split(" ");

  if (arrayCreditCard.length === 4) {
    for (let index = 0; index < arrayCreditCard.length; index++) {
      if (arrayCreditCard[index].length !== 4) {
        return false;
      }
    }
    return true;
  }



  return false;
};


// Comprobar textarea
const validateTextarea = () => { };
