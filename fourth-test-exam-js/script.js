const changeBGColor = (id) => {
  document.getElementById(id).style.backgroundColor = "red";
};

const validateForm = (ev) => {
  const personalData = validatePersonalData();
  const personalAddress = validatePersonalAddress();
  const personalBankAccount = validatePersonalBankAccount();

  if (personalData && personalAddress && personalBankAccount) {
    alert("Formulario validado correctamente, estás aprobado");
    return;
  }
  alert("Revisa los inputs, el formulario contiene errores...");
  ev.preventDefault();
};

const checkEmptyInput = (inputValue) => {
  if (inputValue !== "") {
    return true;
  }

  return false;
};

const checkNumberInput = (inputValue) => {
  if (isNaN(inputValue) || inputValue <= 0) {
    return false;
  }
  return true;
};

const checkRadioInput = (classname) => {
  const radioInputs = document.querySelectorAll(`.${classname} input`);

  if (radioInputs[0].checked || radioInputs[1].checked) {
    return true;
  }

  return false;
};

// personalData

const validatePersonalData = () => {
  const firstNameInput = document.querySelector("#firstName").value;
  const lastNameInput = document.querySelector("#lastName").value;
  const ageInput = document.querySelector("#age").value;

  const firstName = checkEmptyInput(firstNameInput);
  const lastName = checkEmptyInput(lastNameInput);
  const age = checkEmptyInput(ageInput);
  const genre = checkRadioInput("genre");
  const email = checkEmail();

  if (firstName && lastName && age && genre && email) {
    return true;
  }

  return false;
};

const checkEmail = () => {
  const email = document.querySelector("#email").value;
  const emailConfirm = document.querySelector("#emailConfirm").value;

  if (email === "" || emailConfirm === "") {
    return false;
  }

  if (email === emailConfirm) {
    return true;
  }

  return false;
};

// personalAddress

const validatePersonalAddress = () => {
  const addressInput = document.querySelector("#address").value;
  const stateInput = document.querySelector("#state").value;
  const zipCodeInput = document.querySelector("#zipCode").value;

  const address = checkEmptyInput(addressInput);
  const state = checkEmptyInput(stateInput);
  const zipCode = checkNumberInput(zipCodeInput);

  if (address && state && zipCode) {
    return true;
  }

  return false;
};

// personalBankAccount

const validatePersonalBankAccount = () => {
  const creditCard = checkCreditCard();
  const creditCardType = checkRadioInput("creditCardType");

  if (creditCard && creditCardType) {
    return true;
  }

  return false;
};

const checkCreditCard = () => {
  const creditCardValue = document.querySelector("#creditCard").value;

  if (creditCardValue === "") {
    return false;
  }

  const arrayCreditCard = creditCardValue.split(" ");

  if (arrayCreditCard.length === 4) {
    for (let index = 0; index < arrayCreditCard.length; index++) {
      if (arrayCreditCard[index].length !== 4) {
        return false;
      }
    }
    return true;
  }

  return false;
};
