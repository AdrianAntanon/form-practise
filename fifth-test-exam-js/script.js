const changeBGColor = id => {
  document.getElementById(id).style.backgroundColor = "red";
};


const validateForm = ev => {
  const personalData = validatePersonalData();
  const personalAddress = validatePersonalAddress();
  const personalBankAccount = validatePersonalBankAccount();
  const moreInfo = validateTextarea();

  if (personalData && personalAddress && personalBankAccount && moreInfo) {
    alert("Formulario validado, estás aprobado.");
    return;
  }

  alert("El formulario contiene errores, has puesto mal algo...");
  ev.preventDefault();
};

const checkEmptyInput = inputValue => {
  if (inputValue !== "") {
    return true;
  }

  return false;
};

const checkNumberInput = inputValue => {
  if (isNaN(inputValue) || inputValue <= 0) {
    return false;
  }

  return true;
};

const checkRadioInput = classname => {
  const inputs = document.querySelectorAll(`.${classname} input`);

  if (inputs[0].checked || inputs[1].checked) {
    return true;
  }

  return false;
};


// Personal data

const validatePersonalData = () => {
  const firstNameInput = document.querySelector("#firstName").value;
  const lastNameInput = document.querySelector("#lastName").value;
  const ageInput = document.querySelector("#age").value;
  const birthdateInput = document.querySelector("#birthdate").value;


  const firstName = checkEmptyInput(firstNameInput);
  const lasttName = checkEmptyInput(lastNameInput);
  const age = checkNumberInput(ageInput);
  const genre = checkRadioInput("genre");
  const email = checkEmail();
  const birthdate = checkEmptyInput(birthdateInput);
  const studies = checkStudies();


  if (firstName && lasttName && age && genre && email && birthdate && studies) {
    return true;
  }

  return false;

};

const checkEmail = () => {
  const email = document.querySelector("#email").value;
  const emailConfirm = document.querySelector("#emailConfirm").value;

  if (email === "" || emailConfirm === "") {
    return false;
  }

  if (email === emailConfirm) {
    return true;
  }

  return false;
};

const checkStudies = () => {
  const inputs = document.querySelectorAll(`.studies input`);

  if (inputs[0].checked || inputs[1].checked || inputs[2].checked || inputs[3].checked) {
    return true;
  }

  return false;
};

// Personal address

const validatePersonalAddress = () => {
  const addressInput = document.querySelector("#address").value;
  const stateInput = document.querySelector("#state").value;
  const zipCodeInput = document.querySelector("#zipCode").value;

  const address = checkEmptyInput(addressInput);
  const state = checkEmptyInput(stateInput);
  const zipCode = checkNumberInput(zipCodeInput);
  const country = checkCountries();


  if (address && state && zipCode && country) {
    return true;
  }

  return false;

};

const checkCountries = () => {
  const inputs = document.querySelectorAll(`.country input`);

  if (inputs[0].checked || inputs[1].checked || inputs[2].checked || inputs[3].checked) {
    return true;
  }

  return false;
};

// Personal bank account

const validatePersonalBankAccount = () => {
  const creditCardType = checkRadioInput("creditCardType");
  const creditCard = checkCreditCard();

  if (creditCard && creditCardType) {
    return true;
  }

  return false;
};

const checkCreditCard = () => {
  const creditCard = document.querySelector("#creditCard").value;

  if (creditCard === "") {
    return false;
  }
  const arrayCreditCard = creditCard.split(" ");
  if (arrayCreditCard.length === 4) {
    for (let index = 0; index < arrayCreditCard.length; index++) {
      if (arrayCreditCard[index].length !== 4) {
        return false;
      }


    }
    return true;
  }

  return false;
};

// textarea

const validateTextarea = () => {
  const textArea = document.querySelector("#moreInfo").value;

  if (textArea === "" || textArea.length <= 10) {
    return false;
  }

  return true;
};