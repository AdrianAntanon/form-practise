const changeBGinputColor = (id) => {
  document.getElementById(id).style.backgroundColor = "red";
};

const validateForm = (ev) => {
  const personalInfo = validatePersonalInfo();
  const addressInfo = validateAddressInfo();
  const bankAccountInfo = validateBankAccount();
  const textareaInfo = validateTextArea();

  if (personalInfo && addressInfo && bankAccountInfo && textareaInfo) {
    alert("Formulario validado, estás aprobado!");
    return;
  }

  alert("El formulario contiene fallos...");
  ev.preventDefault();
};

// Comprobar información personal
const validatePersonalInfo = () => {
  const firstName = document.querySelector("#firstName");
  const lastName = document.querySelector("#lastName");
  const fname = checkName(firstName, "El nombre debe rellenarse");
  const lname = checkName(lastName, "Los apellidos deben rellenarse");
  const age = checkAge();
  const genre = checkGenre();
  const email = checkEmail();
  const studies = checkStudies();

  if (fname && lname && age && genre && email && studies) {
    return true;
  }

  return false;
};
const checkName = (name, text) => {
  name.style.backgroundColor = "white";

  if (name.value !== "") {
    return true;
  }

  alert(text);
  changeBGinputColor(name.id);
  return false;
};

const checkAge = () => {
  const age = document.querySelector("#age");
  age.style.backgroundColor = "white";

  if (age.value <= 0 || isNaN(age.value)) {
    alert("La edad no es válida");
    changeBGinputColor(age.id);

    return false;
  }
  return true;
};

const checkGenre = () => {
  const genre = document.querySelectorAll(".genre input");

  if (genre[0].checked || genre[1].checked) {
    return true;
  }

  alert("Seleccione un género, por favor");
  return false;
};

const checkEmail = () => {
  const email = document.querySelector("#email");
  const emailConfirm = document.querySelector("#emailConfirm");
  email.style.backgroundColor = "white";
  emailConfirm.style.backgroundColor = "white";

  if (email.value == "" || emailConfirm.value == "") {
    alert("Los mails se deben rellenar");
    changeBGinputColor(emailConfirm.id);
    changeBGinputColor(email.id);
    return false;
  }

  if (email.value === emailConfirm.value) {
    return true;
  }

  alert("Los mails no coinciden");
  changeBGinputColor(emailConfirm.id);
  changeBGinputColor(email.id);
  return false;
};

const checkStudies = () => {
  const education = document.querySelectorAll(".education input");

  if (
    education[0].checked ||
    education[1].checked ||
    education[2].checked ||
    education[3].checked
  ) {
    return true;
  }

  alert("Seleccione al menos un tipo de educación, por favor");
  return false;
};

// Comprobar dirección

const validateAddressInfo = () => {
  const address = document.querySelector("#address");
  const state = document.querySelector("#state");

  const zipCode = checkZipCode();
  const lAddress = checkName(address, "La dirección debe rellenarse");
  const lState = checkName(state, "La población debe seleccionarse");
  const country = checkCountry();

  if (zipCode && lAddress && lState && country) {
    return true;
  }

  return false;
};

const checkZipCode = () => {
  const zipCode = document.querySelector("#zipCode");
  zipCode.style.backgroundColor = "white";

  if (!zipCode.value) {
    alert("Introduce el código postal");
    changeBGinputColor(zipCode.id);
    return false;
  }

  return true;
};

const checkCountry = () => {
  const genre = document.querySelectorAll(".country input");

  if (
    genre[0].checked ||
    genre[1].checked ||
    genre[2].checked ||
    genre[3].checked
  ) {
    return true;
  }

  alert("Seleccione un país, por favor");
  return false;
};

// Comprobar datos bancarios
const validateBankAccount = () => {
  const creditCard = checkCreditCard();
  const typeCC = checkCreditCardType();

  if (creditCard && typeCC) {
    return true;
  }

  return false;
};

const checkCreditCard = () => {
  const creditCard = document.querySelector("#creditCard");
  creditCard.style.backgroundColor = "white";

  const numCreditCard = creditCard.value;

  let array = numCreditCard.split(" ");

  if (array.length === 4) {
    for (let index = 0; index < array.length; index++) {
      if (array[index].length !== 4) {
        alert("La targeta no es correcta");
        return false;
      }
    }
    return true;
  }

  changeBGinputColor(creditCard.id);
  alert("La targeta no es correcta");

  return false;
};

const checkCreditCardType = () => {
  const cardType = document.querySelectorAll(".cardType input");
  if (cardType[0].checked || cardType[1].checked) {
    return true;
  }
  alert("Seleccione el tipo de targeta, por favor");
  return false;
};

// Comprobar textarea

const validateTextArea = () => {
  const textarea = document.querySelector("#moreInfo");
  textarea.style.backgroundColor = "white";

  if (textarea.value.length >= 10) {
    return true;
  }
  alert("El apartado de información debe de contener al menos 10 carácteres");
  changeBGinputColor(textarea.id);
  return false;
};
